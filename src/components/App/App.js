import React from 'react';
import './App.scss';
import Gallery from '../Gallery';
import FontAwesome from 'react-fontawesome';

class App extends React.Component {
  static propTypes = {
  };

  constructor() {
    super();
    this.state = {
      tag: 'puppy',
      favMode: false, //flag for fav mode
      numFavs: 0
    };
    this.numFavs = this.numFavs.bind(this)// bind clone
  }
numFavs = (num) => {
    this.setState({numFavs: num});
  }
  toggleFav() {
  this.setState({favMode: !this.state.favMode});
  }
  render() {
    return (
      <div className="app-root">
        <div className="app-header">
          <h2>Flickr Gallery</h2>
          <input className="app-input" onChange={event => this.setState({tag: event.target.value})} value={this.state.tag}/>
          <div><button className="but" onClick={() => this.toggleFav()}><FontAwesome className="image-icon" name="star icon" title="favorites"/>{this.state.favMode ? 'Back' : 'Favorites'}</button></div>
        </div>
        <Gallery tag={this.state.tag} fav={this.state.favMode} numFavs={this.numFavs} favMode={this.state.favMode}/>
      </div>
    );
  }
}

export default App;
