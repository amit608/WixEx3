import React from 'react';
import PropTypes from 'prop-types';
import axios from 'axios';
import Image from '../Image';
import './Gallery.scss';


class Gallery extends React.Component {
  static propTypes = {
    tag: PropTypes.string,
  };

  constructor(props) {
    super(props);
    this.state = {
      images: [],
      galleryWidth: this.getGalleryWidth(),
      loadpics: 100, //num of pics to load
      finished: false, //if finished loading. (a stopper for infi scroll)
      favs: []
    };
    this.clone = this.clone.bind(this)// bind clone
    this.addFav = this.addFav.bind(this)// bind fav
  }

  getGalleryWidth(){
    try {
      return document.body.clientWidth;
    } catch (e) {
      return 1000;
    }
  }
  getImages(tag, picsNum) {
    this.setState({finished: false}); //is loading for infi scroll
    const getImagesUrl = `services/rest/?method=flickr.photos.search&api_key=522c1f9009ca3609bcbaf08545f067ad&tags=${tag}&tag_mode=any&per_page=${picsNum}&format=json&safe_search=1&nojsoncallback=1`;
    const baseUrl = 'https://api.flickr.com/';
    axios({
      url: getImagesUrl,
      baseURL: baseUrl,
      method: 'GET'
    })
      .then(res => res.data)
      .then(res => {
        if (
          res &&
          res.photos &&
          res.photos.photo &&
          res.photos.photo.length > 0
        ) {
          this.setState({images: res.photos.photo});
          this.setState({finished: true}); //finished loading
        }
      });
  }

  componentDidMount() {
    this.getImages(this.props.tag, this.state.loadpics);
    this.setState({
      galleryWidth: document.body.clientWidth
    });
    window.addEventListener('scroll', this.onScroll, false); //infi scroll listner for scroll
    let d = localStorage.getItem('favs');
    if(d != null && d != '') {
      this.setState({favs: JSON.parse(d)}); //loading favs from memory
    }

  }
  componentWillReceiveProps(props) {
   this.getImages(props.tag, 100); //infi scroll back to 100 pics after new tag search
 //localStorage.clear();
  }

  onScroll = () => { //infi scroll eventer
    if (((window.innerHeight + window.scrollY) >= (document.body.offsetHeight - 300)) && this.state.finished)
     {
       this.setState({loadpics: this.state.loadpics+50})
       this.getImages(this.props.tag, this.state.loadpics);
     }
  }

  clone(msg){ //clone
    let b=[];
    for(var i = 0; i< this.state.images.length;++i){
      let temp = JSON.parse(JSON.stringify(this.state.images[i]));//we have to clone the element and not point to it
      b.push(this.state.images[i]);
      if(msg.id == this.state.images[i].id){
        temp.cloned = temp.cloned+'cloned';
        b.push(temp);
      }
    }
    this.setState({images: b});
  }
  addFav = (msg) => {
      let c = this.state.favs;
      let flag = 0;
      //searches if its already in the favorites. if so, delete it
    for(var i = 0; i< this.state.favs.length;++i) {
      if (msg.id == c[i].id) {
        c = c.slice(0, i).concat(c.slice(i+1), c.length);
        console.log('nothing');
        flag = 1;
      }
    }
    if(flag == 0) { //if not in favorites add it
      c.push(msg);
    }
      this.setState({favs: c});
      localStorage.setItem('favs', JSON.stringify(c));
      this.props.numFavs(c.length);
  }

isFav(id) {
  for(var i = 0; i< this.state.favs.length;++i){
    if(id == this.state.favs[i].id){
      return true;
    }
  }
  return false;
}
  render() {
    //regular mode
    if(!this.props.favMode) {
      return (

        <div className="gallery-root">
          {this.state.images.map(dto => {
            //clone - added dto.cloned in order to avoid same key for two objects
            return (<Image key={'image-' + dto.cloned + dto.id} clone={this.clone} dto={dto}
                           galleryWidth={this.state.galleryWidth} addFav={this.addFav} isfav={this.isFav(dto.id)}/>
            );
          })}
        </div>
      );
    }
    //favorites mode
    else {
      return (

        <div className="gallery-root">

          {this.state.favs.map(dto => {
            if(dto.id != null){ //avoid ghost images
            //clone - added dto.cloned in order to avoid same key for two objects
            return (<Image key={'image-' +dto.cloned+dto.id} clone = {this.clone} dto={dto} galleryWidth={this.state.galleryWidth} addFav={this.addFav} isfav={this.isFav(dto.id)}/>
            );
            }
          })}
        </div>
      );

    }
  }
}

export default Gallery;
