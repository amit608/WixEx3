import React from 'react';
import PropTypes from 'prop-types';
import FontAwesome from 'react-fontawesome';
import './Image.scss';

class Image extends React.Component {
  static propTypes = {
    dto: PropTypes.object,
    galleryWidth: PropTypes.number
  };

  constructor(props) {
    super(props);
    this.calcImageSize = this.calcImageSize.bind(this);
    this.state = {
      size: 200,
      flip: false //flip flag
    };
    this.flipimg = this.flipimg.bind(this); //ME binding this for func
  }

  calcImageSize() {
    const {galleryWidth} = this.props;
    const targetSize = 200;
    const imagesPerRow = Math.round(galleryWidth / targetSize);
    const size = (galleryWidth / imagesPerRow);
    this.setState({
      size
    });
  }

  componentDidMount() {
    this.calcImageSize();
  }
  urlFromDto(dto) {
    return `https://farm${dto.farm}.staticflickr.com/${dto.server}/${dto.id}_${dto.secret}.jpg`;
  }

  flipimg() { //flip func
 this.setState({flip: !this.state.flip});
 }
  childClone(){ //clone added child clone function
    this.props.clone(this.props.dto);
  }
  childFav(){ //clone added child clone function
    this.props.addFav(this.props.dto);
  }
  icon() { //decide the icon for the favorites
    return this.props.isfav ? 'times-circle' : 'star';
  }
  render() {
      return (
        <div
          className={this.state.flip ? 'image-root yes' : 'image-root no'}
          style={{
            backgroundImage: `url(${this.urlFromDto(this.props.dto)})`,
            width: this.state.size + 'px',
            height: this.state.size + 'px'
          }}
        > <div><div>
            <FontAwesome onClick={() => this.flipimg()} className="image-icon" name="arrows-alt-h" title="flip"/>
          <FontAwesome onClick={() => this.childClone()} className="image-icon" name="clone" title="clone"/></div><div>
            <a href={`${this.urlFromDto(this.props.dto)}`} data-lightbox="gallery" data-title={this.props.dto.title}>
              <FontAwesome className="image-icon" name="expand" title="expand"/></a>
            <FontAwesome onClick={() => this.childFav()} className="image-icon" name={`${this.icon()} icon`} title="favorites"/>
        </div>
        </div>
        </div>

      );
  }
}

export default Image;
